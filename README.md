# Фронтенд-часть системы управления объектами Архитектуры Одессы

Для работы использует [NgAdmin](http://ng-admin-book.marmelab.com), основанный на [Angular](https://angularjs.org/).

- [Планы и генеральное видение](ROADMAP.md)
- [Баги и фичи](https://bitbucket.org/archodessa-web-team/archodessa-frontend/issues)

## Запуск приложения

Перед запуском приложения убедитесь, что у вас на той же машине запущен бэкенд [archodessa-backend](https://bitbucket.org/archodessa-web-team/archodessa-backend).

```bash
npm i // устанавливаем зависимости
npm start // запускаем сервер
```

## Разработка

NgAdmin позволяет просто описывать модели и связи между ними, генерируя на основе этого админку. Для понимания принципов работы посмотрите на ```src/app.js``` и в папку ```models```. После этого открывайте документацию по [NgAdmin](http://ng-admin-book.marmelab.com) и [Angular](https://angularjs.org/) и начинайте писать.

## Я могу! Я хочу!

Можешь? Хочешь? Знаешь, как сделать лучше? Заметил говно и умеешь исправить?

Открой [предложение](https://bitbucket.org/archodessa-web-team/archodessa-frontend/issues) или пришли патч.
