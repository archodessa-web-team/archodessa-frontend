export default function (nga) {

    var model = nga.entity('architecturalType').label('Типы');

    model.listView()
      .title('Архитектурные типы')
      .fields([
          nga.field('name')
          .label('Название')
      ])
      .listActions(['edit', 'delete']);

    model.creationView()
      .title('Создание архитектурного типа')
        .fields([
          nga.field('name')
            .label('Название')
        ]);
    // use the same fields for the editionView as for the creationView
    model.editionView()
      .title('Редактирование типа «{{ entry.values.name }}»')
      .fields(model.creationView().fields());

    return model;
}
