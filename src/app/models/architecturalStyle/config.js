export default function (nga) {

    var model = nga.entity('architecturalStyle').label('Стили');

    model.listView()
      .title('Архитектурные стили')
      .fields([
          nga.field('name')
            .label('Название')
      ])
      .listActions(['edit', 'delete']);

    model.creationView()
      .title('Создание стиля')
      .fields([
        nga.field('name')
            .label('Название')
      ]);
    // use the same fields for the editionView as for the creationView
    model.editionView()
      .title('Редактирование стиля «{{ entry.values.name }}»')
      .fields(model.creationView().fields());

    return model;
}
