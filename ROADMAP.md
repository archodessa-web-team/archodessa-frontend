# Планы и генеральное видение

Первая версия проекта строится на MongoDB и Sails.js со стороны бэкенда и Angular и NgAdmin со стороны фронтенда.

Sails.js и NgAdmin позволяют легко описывать модели данных и связи между ними.

## ОБЪЕКТ

На данный момент у нас есть примитивная структура объекта (т.е. здания или дома):

- Название
- Локация/Адрес (с возможностью указывать несколько локаций в разные времена)
- *Краткое описание*
- Координаты
- Тип объекта
- Стиль объекта
- Этажность
- Архитектурные элементы
- Автор (Архитектор)
- *Инженер*
- *Подрядчик*
- *Тэги*

Это не четкая структура, но это отправная точка.
Поля, выделенные курсивом, еще не реализованы.

### Планы по развитию

Поле адреса должно быть достаточно гибким для различных форматов записи адреса

Поле координат должно представлять собой карту, на которой можно отметить место здания

«Тип объекта» и «Стиль объекта» должны быть древовидным выпадающим списком

«Архитектурные элементы» должны добавляться с помощью автодополнения, а значения, которых нет в БД, должны добавляться автоматически.

### Второй этап

Вторым этапом необходимо добавить следующие данные:

- *Годы/События*
- *Изображения*

«Годы/События» есть сложная форма, которая будет описана позже.

Необходимо реализовать возможность вести «историю» по любому параметру, например, указать, что до 20 века задние было общественным, а потом стало жилым

На существующую систему БД нужно прикручивать поиск, например GraphQL или ElasticSearch.

### Третий этап

Третьим этапом необходимо добавить следующие данные:

- *Статус*
    - *Значение (выпадашка)*
    - *Номер в реестре*
    - *Когда принят под охрану*




# Изображения

Изображения должны маркироваться, к какому элементу городского дизайна они относятся. Т.е должны ставиться метки «Что изображено на фото?», например:

- Дом такой-то, входной флигель, синий, деревянный
- Люк ОРТ, 1918 год, чугуний
- Табличка-вывеска, медная


# Предположительная структура объекта

```bash
Наименование
Адрес (история)
  Район (пока не критичен)
    Новое назание (Приморский)
    Старое название (Жовтневый)
  Тип улицы (хорошо бы объединить с названием улицы)
    Проспект
    Бульвар
    Площадь
    Улица
    Переулок
  Название улицы
    Новое (Нежинская)
    Старое (Франца Меринга)
  Номер дома
    11в/2
    -
Координаты (сделать карту, чтобы отмечать кликом место)
  Широта
  Долгота
Тип объекта (история)
  Жилое здание
    Жилые здания секционного типа
    Блокированные жилые дома
    Жилые здания галерейного типа
    Жилые здания коридорного типа
    Жилые индивидуальные дома
    Мобильные дома.
  Общественное здание
    Здания для образования, воспитания и подготовки кадров.
      Детские дошкольные учреждения общего типа, специализированные, оздоровительные и объединенные с начальной школой.
      Общеобразовательные и специализированные школы-интернаты, межшкольные учебно-производственные комбинаты.
      Профессионально-технические училища и учебные заведения для подготовки и переподготовки рабочих кадров.
      Средние специальные учебные заведения.
      Высшие учебные заведения.
      Учебные заведения для подготовки и повышения квалификации специалистов.
      Внешкольные учреждения.
    Здания для научно-исследовательских учреждений, проектных и общественных организаций и управления.
    Здания и сооружения для здравоохранения и отдыха.
      Лечебные со стационаром, амбулаторно-поликлинические, аптеки, молочные кухни, бальнео-и грязелечебницы.
      Санатории, санатории-профилактории
      Учреждения отдыха и туризма
    Физкультурно-оздоровительные и спортивные здания
      Открытые спортивно-физкультурные сооружения.
      Крытые здания и сооружения.
      Физкультурно-спортивные и оздоровительные комплексы.
    Культурно-просветительные и зрелищные учреждения.
      Библиотеки.
      Музеи и выставки.
      Клубные здания (клубы, дома и дворцы культуры, центры досуга и др.).
      Зрелищные здания (театры, концертные залы, кинотеатры, цирки и др.).
    Предприятие торговли, общественного питания и бытового обслуживания.
      Здания для предприятий розничной торговли.
      Здания для предприятий общественного питания
      Здания для предприятий бытового обслуживания
    Вокзалы всех видов транспорта.
    Здания для коммунального хозяйства
      Здания для гражданских обрядов.
      Жилищно-эксплуатационные.
      Общественные уборные.
      Пожарные депо.
    Многофункциональные здания и комплексы, включающие помещения различного назначения.
    Добавить
  Производственные здания
    Предприятия машиностроения.
    Предприятия химической промышленности.
    Нефтеперерабатывающее производство.
    Текстильное производство.
    Типографское производство.
    Деревообрабатывающие предприятия.
    Металлургические предприятия.
    Фармацевтическое производство.
    Табачное производство.
    Пивоваренное производство.
    Ликеро-водочное производство.
    Производство безалкогольных напитков.
    Хлебопекарное и макаронное производство.
    Кондитерские фабрики.
    Предприятия мясо-молочной промышленности и т.
  Объект ландшафтной архитектуры
    сад
    сквер
    бульвары
    набережная
    пляж
    городской центр
    территория школ, детских учреждений, промпредприятий и различных ведомств
  Объект городской среды
    торговля и питание
      киоски
      павильоны
      стенды и пр.
    связь и информация
      телефоны-автоматы
      почтовые ящики
      ин­формационные установки
    транспорт
      навесы и павильоны у остановок
      опоры контактных сетей
      светофоры
      дорожные знаки
    коммунально-хозяйственные службы
      фонари и др. установки освещения
      емкости для мусора
      туалеты и пр.
    мебель и оборудование площадок во дворах, парках и скверах
      детские
      спортивные
      для животных и пр.
    устройства разграничения зон улиц и площадей
      ограды
      барье­ры
      турникеты
    визуальная информация
      указатели
      названия улиц и площадей
      номера домов
      табло
      пиктограммы и пр
  Инженерное сооружение
    Мост
    Маяк
    дороги
    линии электропередач
    трубопроводы
    каналы
    линии связи
Стиль (что делать, если стиль фасада один, а внутри другой?)
  Эклектика
  Ар-нуво
  готика
  неоготика
  романский
  неороманский
  псевдорусский
  барокко
  рококо
  ренессанс
  классицизм
  ампир
  неоклассицизм
  Конструктивизм
  Добавить button / https://goo.gl/Wrtt9Y
Этажность дома (история)
  число этажей
  и точка
Конструкции (история) https://goo.gl/DXp20Q
  подвал
  цокольный этаж
  мансарда
  антресоль
  аттиковый этаж
  мезонин
  и т п
```
